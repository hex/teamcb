var express = require('express');
var app = express();
var mysql = require('mysql');

var pool  = mysql.createPool({
  connectionLimit : 10,
  host            : 'v1.octozon.com',
  user            : 'teamcb',
  password        : 'qu48Fd_8'
});


app.get('/api/', function (req, res) {
  res.send('Hello World!');
});

app.get('/api/newslist', function (req, res) {
	var data = {
		articles : [
        		{ "title": "La primera noticia", "author":"Admin", "datex": new Date().toString(), content: "hola la noticia pimpam..."},
        		{ "title": "La Segona noticia", "author":"Admin", "datex": new Date().toString(), content: "weee hola la noticia pimpam..."}
			]
	}
  res.send(JSON.stringify(data));
});

app.get('/api/boards', function (req, res) {
	var data = {
            boards : [
                { "gid": 1, "bid" : 1, "title": "Article", "content": "Primera seccio..."},
                { "gid": 1, "bid" : 2, "title": "Seccio 2!", "content": "Segona seccio..."},
                { "gid": 2, "bid" : 3, "title": "Seccio 3!", "content": "Tercera seccio..."},
                { "gid": 2, "bid" : 4, "title": "Seccio 4!", "content": "Cuarta seccio..."},
                { "gid": 3, "bid" : 5, "title": "Seccio 5!", "content": "Cinquena seccio..."}
            ]
        }
  res.send(JSON.stringify(data));
});

app.use(express.static('public'));

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

