import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';

import Newslist from './components/news/newslist';
import Forum from './components/forum/forum';
import Menu from './components/main/menu';


ReactDOM.render(
	(
		<Router history={browserHistory}>
			<Route path="/" component={Newslist} />
			<Route path="/forum" component={Forum} />
			<Route path="*" component={Newslist} />
		</Router>
	),
document.getElementById('container'));

ReactDOM.render(<Menu />, document.getElementById('menu'));