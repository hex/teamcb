import React from 'react';
import Article from './article';

export default class Newslist extends React.Component {
	constructor() {
		super();
		this.state = {
			articles : []
		}
	}

	//CREACIO
	componentDidMount() {
	}


	componentWillMount() {
		/*
		console.log("willmount");
		var art = [
        		{ "title": "La primera noticia", "author":"Admin", "datex": new Date().toString(), content: "hola la noticia pimpam..."},
        		{ "title": "La Segona noticia", "author":"Admin", "datex": new Date().toString(), content: "weee hola la noticia pimpam..."}
			];
		this.setState({
			articles : art 
		}); */

		fetch('http://localhost:3000/api/newslist', {
			method : 'get'
		}).then((response) => {
			return response.json();
		}).then((data) => {
			this.setState({ articles : data.articles });
		}).catch((err) => {
			if (err) {
				console.log(err);
			}
		})

	}

	//ACTUALITZACIO
	/*
	//Es pot comprobar els props anteriors amb els actuals
	componentWillRecibeProps(nextProps) {

	}

	// Que no executi el metode render perque les props no requereixen execucio.
	sholdComponentUpdate(nextProps, nextState) {
	}

	componentWillUpdate(nextProps, nextState) {
	}

	componentDidUpdate(prevProps, prevState) {
	}
	*/

	//DESTRUCCIO
	/*
	//Usat per lliberar memoria
	componentWillUnmount() {
	
	}
	*/

	//MIXINS
	/*
	//Compartir informacio entre components (NO ESTAN DISPONIBLES PER ES6 PERO USA CONSTRUCTOR)
	
	var LogMixin = {
		componentDIdMount: fuunction () {
			console.log("hola")
		}
	}

	var Componente = React.createClass({
		this.mixins: [LogMixin] // ARRAY DE MIXINS
		render () {
			return...
		}
	})
	*/

    render () {
        return (
            <ul>
            {
            	this.state.articles.map(function (pro, i) {
                	return( <Article key={pro.title} title={pro.title} author={pro.author} datex={pro.datex} content={pro.content} /> )
            	})
            }
            </ul>
        )
    }
}