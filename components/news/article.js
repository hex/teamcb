import React from 'react';

export default class Article extends React.Component {
	onClick(ev) {
		console.log("Clicat");
	}

    render() {
        return (
        	<div onClick={this.onClick}>
        			<h1>{this.props.title}</h1>
        			<h2><b>{this.props.author}</b> - {this.props.datex}</h2>
        			{this.props.content}
        	</div>
        )
    }
}