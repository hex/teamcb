import React from 'react';

export default class Menu extends React.Component {
	componentWillMount() {
    var mainurl = "http://localhost:3000/";
		this.setState({
			menu : [
        		{ "mmid" : 1, "title": "Noticies", "url" : mainurl + "#" },
        		{ "mmid" : 2, "title": "Forum", "url" : mainurl + '#/forum'}
			]
		});
	}

    render () {
        return (
            <div>
            	<h1>MENU</h1>
               {
                  this.state.menu.map((pro, i) => {
               		 return (
                     <div key={pro.mmid + "Menu"}><a href={pro.url}>{pro.title}</a></div>
               		 )
                  })
               }
            </div>
        )
    }
}