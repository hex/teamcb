import React from 'react';
import Messages from './messages';

export default class Forumboards extends React.Component {
    constructor() {
        super();
        this.state = {
            boards : []
        }
    }

    componentWillMount() {
        fetch('http://localhost:3000/api/boards', {
            method : 'get'
        }).then((response) => {
            return response.json();
        }).then((data) => {
            this.setState({ boards : data.boards });
        }).catch((err) => {
            if (err) {
                console.log(err);
            }
        })
    }

    render() {
        return (
        	<div>
                {
                    this.state.boards.map((el) => {
                        {
                            if ( el.gid === this.props.gid ) {
                                return (
                                    <div key={el.bid}> BOARD: {el.title}<br />
                                        <Messages bid={el.bid} />
                                    </div> 
                                )
                            }
                        }
                    })
                }
        	</div>
        )
    }
}