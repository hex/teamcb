import React from 'react';
import Boards from './boards';

export default class Forum extends React.Component {
	componentWillMount() {
		this.setState({
			forumgroups : [
        		{ "gid" : 1, "group": "General"},
        		{ "gid" : 2, "group": "Important"},
        		{ "gid" : 3, "group": "Mercat"}
			]
		});
	}

    render () {
        return (
            <div>
            	<h1>FOROS</h1>
               {
                this.state.forumgroups.map((pro, i) => {
               		return (
               		  <div key={pro.gid}>
               		    GRUP: { pro.group } <br />
    					        <Boards gid={pro.gid} />
    					      </div>
               		)
               })
              }
            </div>
        )
    }
}