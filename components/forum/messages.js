import React from 'react';

export default class Forumboards extends React.Component {
    componentWillMount() {
        this.setState({
            messages : [
                { "gid": 1, "bid" : 1, "mid": 1, "title": "MISATJE 1", "content": "Contingut 1 misatje.."},
                { "gid": 1, "bid" : 1, "mid": 2, "title": "MISATJE 2", "content": "Contingut 2 misatje.."},
                { "gid": 1, "bid" : 2, "mid": 1, "title": "MISATJE 3", "content": "Contingut 3 misatje..."},
                { "gid": 1, "bid" : 2, "mid": 2, "title": "MISATJE 4", "content": "Contingut 4 misatje..."}
            ]
        });
    }

    render() {
        return (
        	<div>
                { this.state.messages.map((el) => {
                    { if ( el.bid === this.props.bid ) {
                        return (
                            <div key={el.mid}>
                            <b>{el.title}</b><br />
                            {el.content}<br />
                            </div> 
                        )
                    } }
                })}
        	</div>
        )
    }
}